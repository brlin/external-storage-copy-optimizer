# External Storage Copy Optimizer(IN DEVELOPMENT)

Workaround the following problems caused by the wrongly calculated and inflexible
dirty page size settings behavior in 64-bit Linux systems:

* Operating system thrashing/shuttering due to the primary memory depletion by
  the dirty pages during large data copy in small memory systems
* Long wait time for storage detachment due to large amount of dirty pages yet
  to be flushed into the external storage
* Inaccurate time estimation of the copy process due to the drive writing speed
  being skewed by the write speed of the primary memory

## What does this do

This program sets the appropriate background and blocking dirty page flush
kernel parameter values according to the estimated/given write speed of the
(relatively) slow external storage drives, waits for the user to signal the end
of the file copy progress, then revert the values back to the system default to
restore the I/O performance of the operating system

