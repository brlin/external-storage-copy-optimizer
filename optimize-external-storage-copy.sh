#!/usr/bin/env bash
# Backup operating environment data to the solid state drive
# 林博仁(Buo-ren, Lin) <Buo.Ren.Lin@gmail.com>
SSD_MEBIBYTES_PER_SECONDS="${SSD_MEBIBYTES_PER_SECONDS:-500}"
RSYNC_SRC_DIR='/home/brlin/files'
RSYNC_DEST_BASEDIR='/media/brlin/external-drive'

set \
    -o errexit \
    -o errtrace

original_dirty_ratio=
original_dirty_background_ratio=
original_dirty_background_bytes=
original_dirty_bytes=

restore_kernel_parameters(){
    if test -n "${original_dirty_background_ratio}" \
        && test "${original_dirty_background_ratio}" -ne 0; then
        printf \
            'Info: Restoring the value of the vm.dirty_background_ratio kernel parameter...\n'
        if ! sysctl -w vm.dirty_background_ratio="${original_dirty_background_ratio}"; then
            printf \
                'Warning: Unable to restore the value of the vm.dirty_background_ratio kernel parameter.\n' \
                1>&2
            return 2
        fi
    fi

    if test -n "${original_dirty_ratio}" \
        && test "${original_dirty_ratio}" -ne 0; then
        printf \
            'Info: Restoring the value of the vm.dirty_ratio kernel parameter...\n'
        if ! sysctl -w vm.dirty_ratio="${original_dirty_ratio}"; then
            printf \
                'Warning: Unable to restore the value of the vm.dirty_ratio kernel parameter.\n' \
                1>&2
            return 2
        fi
    fi

    if test -n "${original_dirty_background_bytes}" \
        && test "${original_dirty_background_bytes}" -ne 0; then
        printf \
            'Info: Restoring the value of the vm.dirty_background_bytes kernel parameter...\n'
        if ! sysctl -w vm.dirty_background_bytes="${original_dirty_background_bytes}"; then
            printf \
                'Warning: Unable to restore the value of the vm.dirty_background_bytes kernel parameter.\n' \
                1>&2
            return 2
        fi
    fi

    if test -n "${original_dirty_bytes}" \
        && test "${original_dirty_bytes}" -ne 0; then
        printf \
            'Info: Restoring the value of the vm.dirty_bytes kernel parameter...\n'
        if ! sysctl -w vm.dirty_bytes="${original_dirty_bytes}"; then
            printf \
                'Warning: Unable to restore the value of the vm.dirty_bytes kernel parameter.\n' \
                1>&2
            return 2
        fi
    fi
}

trap_err(){
    printf \
        'Error: The program encountered an unhandled error and is prematurely terminated.\n' \
        1>&2
}
trap trap_err ERR

trap_int(){
    printf \
        'Warning: Received the SIGINT signal, terminating...\n' \
        1>&2
    exit 3
}
trap trap_int INT

trap_exit(){
    if ! restore_kernel_parameters; then
        printf \
            'Warning: Unable to restore the kernel parameters.\n' \
            1>&2
    fi
}
trap trap_exit EXIT

flag_required_command_test_failed=false
required_commands=(
    bc
    date
    rm
    rsync
    sysctl
)
for command in "${required_commands[@]}"; do
    if ! command -v "${command}" >/dev/null; then
        flag_required_command_test_failed=true
        printf \
            'Error: This program requires the "%s" command to be available in your command search PATHs.\n' \
            "${command}" \
            1>&2
    fi
done
if test "${flag_required_command_test_failed}" == true; then
    printf \
        'Error: Required command test failed.\n' \
        1>&2
    exit 1
fi

printf \
    'Info: Checking running user...\n'
if test "${EUID}" -ne 0; then
    printf \
        'Error: This program requires to be run as the superuser(root).\n' \
        1>&2
    exit 1
fi

printf \
    'Info: Keeping the original kernel parameters for restoration...\n'
if ! original_dirty_background_ratio="$(
    sysctl -n vm.dirty_background_ratio
    )"; then
    printf \
        'Error: Unable to read the original value of the vm.dirty_background_ratio kernel parameter.\n' \
        1>&2
    exit 2
fi
printf \
    'Info: The original value of the vm.dirty_background_ratio kernel parameter is "%s".\n' \
    "${original_dirty_background_ratio}"

if ! original_dirty_ratio="$(
    sysctl -n vm.dirty_ratio
    )"; then
    printf \
        'Error: Unable to read the original value of the vm.dirty_ratio kernel parameter.\n' \
        1>&2
    exit 2
fi
printf \
    'Info: The original value of the vm.dirty_ratio kernel parameter is "%s".\n' \
    "${original_dirty_ratio}"

if ! original_dirty_background_bytes="$(
    sysctl -n vm.dirty_background_bytes
    )"; then
    printf \
        'Error: Unable to read the original value of the vm.dirty_background_bytes kernel parameter.\n' \
        1>&2
    exit 2
fi
printf \
    'Info: The original value of the vm.dirty_background_bytes kernel parameter is "%s".\n' \
    "${original_dirty_background_bytes}"


if ! original_dirty_bytes="$(
    sysctl -n vm.dirty_bytes
    )"; then
    printf \
        'Error: Unable to read the original value of the vm.dirty_bytes kernel parameter\n' \
        1>&2
    exit 2
fi
printf \
    'Info: The original value of the vm.dirty_bytes kernel parameter is "%s".\n' \
    "${original_dirty_bytes}"

printf \
    'Info: Determining proper dirty page flushing behavior parameters...\n'
ssd_bytes_per_seconds="$((SSD_MEBIBYTES_PER_SECONDS * 1024 * 1024))"
if ! proper_dirty_background_bytes="$(
    bc <<<"scale = 0; ${ssd_bytes_per_seconds} * 0.2 / 1"
    )"; then
    printf \
        'Error: Unable to determine the proper vm.dirty_background_bytes kernel parameter value.\n' \
        1>&2
    exit 2
fi
printf \
    'Info: The proper vm.dirty_background_bytes kernel parameter value determined to be "%s".\n' \
    "${proper_dirty_background_bytes}"

if ! proper_dirty_bytes="$(
    bc <<<"scale = 0; ${ssd_bytes_per_seconds} * 2"
    )"; then
    printf \
        'Error: Unable to determine the proper vm.dirty_bytes kernel parameter value.\n' \
        1>&2
    exit 2
fi
printf \
    'Info: The proper vm.dirty_bytes kernel parameter value determined to be "%s".\n' \
    "${proper_dirty_bytes}"

printf \
    'Info: Optimizing dirty page flush behavior...\n'
if ! sysctl -w vm.dirty_background_bytes="${proper_dirty_background_bytes}"; then
    printf \
        'Error: Unable to write the optimized value of the vm.dirty_background_bytes kernel parameter.\n' \
        1>&2
    exit 2
fi
if ! sysctl -w vm.dirty_bytes="${proper_dirty_bytes}"; then
    printf \
        'Error: Unable to write the optimized value of the vm.dirty_bytes kernel parameter.\n' \
        1>&2
    exit 2
fi

printf \
    'Info: Determining the backup destination directory path...\n'
if ! datestamp="$(date +%Y%m%d)"; then
    printf \
        'Error: Unable to determine the datestamp for the backup destination directory name.\n' \
        1>&2
    exit 2
fi

rsync_destdir="${RSYNC_DEST_BASEDIR}/brlin-business.${datestamp}"
printf \
    'Info: The backup destination directory path determined to be "%s".\n' \
    "${rsync_destdir}"

printf \
    'Info: Creating backup...\n'
rsync_opts=(
    --progress

    # NOTE: Two occurances is meaningful
    --human-readable
    --human-readable

    --times
    --perms
    --recursive
)
if test -v SUDO_USER; then
    rsync_opts+=(--chown "${SUDO_USER}:${SUDO_USER}")
fi
backup_start_time_epoch="$(date +%s)"
if ! \
    rsync \
        "${rsync_opts[@]}" \
        "${RSYNC_SRC_DIR}" \
        "${rsync_destdir}"; then
    printf \
        'Error: Unable to create backup.\n' \
        1>&2
    exit 3
fi

printf \
    'Info: Synchronizing the cached writes to persistent storage...\n'
if ! sync; then
    printf \
        'Error: Unable to synchronize the cached writes to persistent storage.\n' \
        1>&2
    exit 4
fi
backup_end_time_epoch="$(date +%s)"
backup_duration_in_seconds="$((backup_end_time_epoch - backup_start_time_epoch))"

backup_duration_days="$((backup_duration_in_seconds / 86400))"
remaining_duration_in_seconds="$((backup_duration_in_seconds % 86400))"
backup_duration_hours="$((remaining_duration_in_seconds / 3600))"
remaining_duration_in_seconds="$((remaining_duration_in_seconds % 3600))"
backup_duration_minutes="$((remaining_duration_in_seconds / 60))"
backup_duration_seconds="$((remaining_duration_in_seconds % 60))"

printf \
    'Info: Elapsed %u days, %u hours, %u minutes, and %u seconds.\n' \
    "${backup_duration_days}" \
    "${backup_duration_hours}" \
    "${backup_duration_minutes}" \
    "${backup_duration_seconds}"

printf \
    'Info: Operation completed without errors.\n'

